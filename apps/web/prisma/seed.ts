import { DAY, PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

async function seed() {
  console.log("prisma seed");
  await prisma.user.upsert({
    where: { email: "foo@bar.com" },
    update: {},
    create: {
      email: "foo@bar.com",
      name: "Foo Bar",
    },
  });

  await prisma.user.upsert({
    where: { email: "Baz@bar.com" },
    update: {},
    create: {
      email: "Baz@bar.com",
      name: "Baz Bar",
    },
  });

  const bi = await prisma.muscle.upsert({
    where: { name: "Biceps" },
    update: {},
    create: {
      name: "Biceps",
    },
  });

  const tri = await prisma.muscle.upsert({
    where: { name: "Triceps" },
    update: {},
    create: {
      name: "Triceps",
    },
  });

  const chest = await prisma.muscle.upsert({
    where: { name: "Chest" },
    update: {},
    create: {
      name: "Chest",
    },
  });

  const lats = await prisma.muscle.upsert({
    where: { name: "Lats" },
    update: {},
    create: {
      name: "Lats",
    },
  });

  const pull = await prisma.workout.create({
    data: {
      name: "Pull",
      user: {
        connect: {
          email: "foo@bar.com",
        },
      },
      exercises: {
        create: [
          {
            name: "Chin Up",
            muscles: {
              create: [
                {
                  muscles: {
                    connect: { id: lats.id },
                  },
                },
                {
                  muscles: {
                    connect: { id: bi.id },
                  },
                },
              ],
            },
          },
          {
            name: "Bicep Curl",
            muscles: {
              create: [
                {
                  muscles: {
                    connect: { id: bi.id },
                  },
                },
              ],
            },
          },
        ],
      },
    },
  });

  const push = await prisma.workout.create({
    data: {
      name: "Push",
      user: {
        connect: {
          email: "foo@bar.com",
        },
      },
      exercises: {
        create: ["Pike Push Up", "Decline Push Up", "Diamond Push Up"].map(
          (name) => ({
            name,
            muscles: {
              create: [
                {
                  muscles: {
                    connect: { id: chest.id },
                  },
                },
                {
                  muscles: {
                    connect: { id: tri.id },
                  },
                },
              ],
            },
          })
        ),
      },
    },
  });

  await prisma.week.create({
    data: {
      name: "Week 1",
      user: {
        connect: {
          email: "foo@bar.com",
        },
      },
      workouts: {
        create: [
          {
            day: DAY.MONDAY,
            workout: {
              connect: { id: pull.id },
            },
          },
          {
            day: DAY.TUESDAY,
            workout: {
              connect: { id: pull.id },
            },
          },
          {
            day: DAY.THURSDAY,
            workout: {
              connect: { id: pull.id },
            },
          },
          {
            day: DAY.FRIDAY,
            workout: {
              connect: { id: pull.id },
            },
          },
        ],
      },
    },
  });
}

seed()
  .catch((e: unknown) => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
