import Link from "next/link";
import React from "react";

export default function ({ children }: { children: React.ReactNode }) {
  return (
    <main className="flex min-h-screen flex-col p-24">
      <Link href="..">Back</Link>
      {children}
    </main>
  );
}
