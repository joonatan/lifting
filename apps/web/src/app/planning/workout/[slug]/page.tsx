import { caller } from "~/server/trpc/router/router";

export default async function Page({ params }: { params: { slug: string } }) {
  const workout = await caller.getWorkoutById(params.slug);

  return (
    <>
      <h1 className="text-3xl">Workout: {workout?.name}</h1>
      <div className="flex flex-col">
        <div>{workout?.createdAt?.toDateString()}</div>
      </div>
    </>
  );
}
