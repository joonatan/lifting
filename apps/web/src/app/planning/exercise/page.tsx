import { caller } from "~/server/trpc/router/router";
import Link from "next/link";

export default async function Page() {
  const exercises = await caller.exercises();

  return (
    <>
      <h1 className="text-3xl mb-8">Exercises</h1>
      <ul className="flex flex-col">
        {exercises.map((e) => (
          <li className="mb-2">
            <Link
              href={`/planning/exercise/${e.id}`}
              key={e.id}
              className="flex underline hover:no-underline focus:no-underline flex-row border-b-2 border-transparent hover:border-slate-800 focus-within:border-slate-800 dark:hover:border-slate-300 dark:focus-within:border-slate-300 duration-200"
            >
              <div className="flex flex-col">
                <div>{e.name}</div>
              </div>
            </Link>
          </li>
        ))}
      </ul>
    </>
  );
}
