import { caller } from "~/server/trpc/router/router";

export default async function Page({ params }: { params: { slug: string } }) {
  const exercise = await caller.getWorkoutById(params.slug);

  // TODO get the number of workouts that use this exercise
  // and backlinks to those workouts
  // TODO allow editing

  return (
    <>
      <h1 className="text-3xl">Exercise: {exercise?.name}</h1>
      <div className="flex flex-col">
        <div>{exercise?.createdAt?.toDateString()}</div>
      </div>
    </>
  );
}
