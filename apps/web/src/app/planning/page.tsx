import { caller } from "~/server/trpc/router/router";
import WorkoutForm from "./WorkoutForm";
import Link from "next/link";

export default async function Page() {
  const workouts = await caller.workouts();
  const exercises = await caller.exercises();

  return (
    <>
      <div>TODO need to list workouts here by the user</div>
      <div>Have a select for the user</div>
      <h2 className="text-xl mb-8">List</h2>
      <ul className="flex flex-col mb-8">
        {workouts.map((w) => (
          <li key={w.id}>
            <Link href={`planning/workout/${w.id}`}>{w.name}</Link>
            <div>{w.createdAt.toDateString()}</div>
            {w.sessions.map((l) => (
              <Link href={`session/${l.id}`} key={l.id}>
                {l.id}
              </Link>
            ))}
          </li>
        ))}
      </ul>
      <h2 className="text-xl mb-8">Mutation</h2>
      <div>Add mutations to post to the list</div>
      <WorkoutForm exercises={exercises} />
    </>
  );
}
