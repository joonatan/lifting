"use client";

import { UseFormRegister, useForm } from "react-hook-form";

type WorkoutFormProps = {
  exercises: Array<{ name: string; id: string }>;
};

type WorkoutFormValues = {
  name: string;
  exercise: string;
};

type NameFormValues = {
  name: string;
};
const NamePart = ({
  register,
}: {
  register: UseFormRegister<NameFormValues>;
}) => {
  return (
    <input
      className="w-full bg-neutral-300 dark:bg-slate-900 p-2"
      {...register("name", { required: true })}
    />
  );
};

const WorkoutForm = ({ exercises }: WorkoutFormProps) => {
  const { register, handleSubmit } = useForm<WorkoutFormValues>();
  const onSubmit = (data: WorkoutFormValues) => console.log(data);

  return (
    <form onSubmit={handleSubmit(onSubmit)} className="flex flex-col gap-4">
      <NamePart register={register} />
      <select
        className="w-full bg-neutral-300 dark:bg-slate-900 p-2"
        {...register("exercise")}
      >
        {exercises.map((e) => (
          <option key={e.id} value={e.id}>
            {e.name}
          </option>
        ))}
      </select>
      <input className="w-full bg-slate-950" type="submit" />
    </form>
  );
};

export default WorkoutForm;
