import { fetchRequestHandler } from "@trpc/server/adapters/fetch";
import { appRouter } from "~/server/trpc/router/router";
import { prisma } from "~/server/trpc";

const handler = (req: Request) =>
  fetchRequestHandler({
    endpoint: "/api/trpc",
    req,
    router: appRouter,
    // TODO for what is this? relative to the one in server/trpc.ts
    createContext: () => ({ prisma }),
  });

export { handler as GET, handler as POST };
