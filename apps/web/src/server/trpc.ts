import { PrismaClient } from "@prisma/client";
import { initTRPC } from "@trpc/server";
import superjson from "superjson";

export const prisma = new PrismaClient();

const createContext = () => {
  return {
    prisma,
  };
};

/**
 * Initialization of tRPC backend
 * Should be done only once per backend!
 */
const t = initTRPC.context<typeof createContext>().create({
  transformer: superjson,
});

/**
 * Export reusable router and procedure helpers
 * that can be used throughout the router
 */
export const router = t.router;
export const publicProcedure = t.procedure;
