/**
 * This is the API-handler of your app that contains all your API routes.
 * On a bigger app, you will probably want to split this file up into multiple files.
 */
import { publicProcedure, router } from "~/server/trpc";
import { z } from "zod";
import { prisma } from "~/server/trpc";

export const appRouter = router({
  // TODO need prisma context for this
  getUser: publicProcedure.query(() => {
    return { id: "1", name: "bob" };
  }),
  userById: publicProcedure.input(z.string()).query(async ({ input, ctx }) => {
    // Retrieve the user with the given ID
    const user = await ctx.prisma.user.findUnique({ where: { id: input } });
    return user;
  }),

  userList: publicProcedure.query(async ({ ctx }) => {
    // Retrieve users from a datasource, this is an imaginary database
    const users = await ctx.prisma.user.findMany();
    return users;
  }),
  // TODO allow filtering by date (future, past etc.)
  // TODO add session include
  workouts: publicProcedure.query(async ({ ctx }) => {
    return await ctx.prisma.workout.findMany({
      include: {
        sessions: true,
      },
    });
  }),
  getWorkoutById: publicProcedure
    .input(z.string())
    .query(async ({ input, ctx }) => {
      return await ctx.prisma.workout.findUnique({ where: { id: input } });
    }),
  exercises: publicProcedure.query(async ({ ctx }) => {
    return await ctx.prisma.exercise.findMany();
  }),
  exerciseById: publicProcedure
    .input(z.string())
    .query(async ({ input, ctx }) => {
      return await ctx.prisma.exercise.findUnique({ where: { id: input } });
    }),
  muscles: publicProcedure.query(async ({ ctx }) => {
    return await ctx.prisma.muscle.findMany();
  }),
  getMusclesById: publicProcedure
    .input(z.string())
    .query(async ({ input, ctx }) => {
      return await ctx.prisma.muscle.findUnique({ where: { id: input } });
    }),
  // TODO add mutations
  greeting: publicProcedure
    // This is the input schema of your procedure
    // 💡 Tip: Try changing this and see type errors on the client straight away
    .input(
      z.object({
        name: z.string().nullish(),
      })
    )
    .query(({ input }) => {
      // This is what you're returning to your client
      return {
        text: `hello ${input?.name ?? "world"}`,
        // 💡 Tip: Try adding a new property here and see it propagate to the client straight-away
      };
    }),
});

// export only the type definition of the API
// None of the actual implementation is exposed to the client
export type AppRouter = typeof appRouter;

export const caller = appRouter.createCaller({ prisma });
